import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import LinearProgress from '@material-ui/core/LinearProgress';
import { Link } from 'react-router-dom'
import './categories.scss';

export default class Categories extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            categories: []
        };
    }

    componentDidMount() {
        fetch("https://www.themealdb.com/api/json/v1/1/categories.php")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        categories: result.categories
                    });
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    render() {
        const { error, isLoaded, categories } = this.state;

        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>
                <LinearProgress color="secondary" />
            </div>;
        } else {
            return (
                <div className="categories">
                    <Grid container className="container" spacing={24}>
                        {categories.map(category => (
                            <Grid item key={category.idCategory} className="card">
                                <Card>
                                    <CardActionArea component={Link} to={"/category/" + category.strCategory}>
                                        <CardMedia
                                            className="thumb"
                                            image={category.strCategoryThumb}
                                            title={category.strCategory}
                                        />
                                        <CardContent>
                                            <Typography variant="h5" gutterBottom>
                                                {category.strCategory}
                                            </Typography>
                                            <Typography component="p" color="textSecondary" className="desc">
                                                {category.strCategoryDescription}
                                            </Typography>
                                        </CardContent>
                                    </CardActionArea>
                                    <CardActions>
                                        <Button size="small" color="primary" component={Link} to={"/category/" + category.strCategory}>View Recipes</Button>
                                    </CardActions>
                                </Card>
                            </Grid>
                        ))}
                    </Grid>
                </div>
            )
        }
    }
}
