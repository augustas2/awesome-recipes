import React, { Component } from 'react';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import LinearProgress from '@material-ui/core/LinearProgress';
import { Link } from 'react-router-dom'
import './category.scss';

export default class Category extends Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            meals: []
        };
    }

    componentDidMount() {
        const { params } = this.props.match;

        fetch("https://www.themealdb.com/api/json/v1/1/filter.php?c=" + params.id)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        meals: result.meals
                    });
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }

    render() {
        const { error, isLoaded, meals } = this.state;

        if (error) {
            return <div>Error: {error.message}</div>;
        } else if (!isLoaded) {
            return <div>
                <LinearProgress color="secondary" />
            </div>;
        } else {
            return (
                <div className="category">
                    <Grid container className="container" spacing={24}>
                        {meals.map(meal => (
                            <Grid item key={meal.idMeal} className="card">
                                <Card>
                                    <CardActionArea component={Link} to={"/recipe/" + meal.idMeal}>
                                        <CardMedia
                                            className="thumb"
                                            image={meal.strMealThumb}
                                            title={meal.strMeal}
                                        />
                                        <CardContent>
                                            <Typography variant="h6" gutterBottom>
                                                {meal.strMeal}
                                            </Typography>
                                        </CardContent>
                                    </CardActionArea>
                                    <CardActions>
                                        <Button size="small" color="primary" component={Link} to={"/recipe/" + meal.idMeal}>View Recipe</Button>
                                    </CardActions>
                                </Card>
                            </Grid>
                        ))}
                    </Grid>
                </div>
            )
        }
    }
}
