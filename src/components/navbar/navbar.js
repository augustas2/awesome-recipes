import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import { Link } from 'react-router-dom'
import './navbar.scss';

export default class Navbar extends Component {
    render() {
        return (
            <div className="navbar">
                <AppBar position="static">
                    <Toolbar>
                        <Typography variant="title" color="inherit" className="title" component={Link} to="/">
                            Awesome Recipes
                        </Typography>
                        <div className="grow" />
                        <div className="search">
                            <div className="searchIcon">
                                <SearchIcon />
                            </div>
                            <InputBase
                                placeholder="Search…"
                                className="input"
                            />
                        </div>
                    </Toolbar>
                </AppBar>
            </div>
        )
    }
}
