import React, { Component } from 'react';
import { usePromiseTracker } from 'react-promise-tracker';
import LinearProgress from "@material-ui/core/LinearProgress";
import './loader.scss';

export default class Loader extends Component {
    const { promiseInProgress } = usePromiseTracker();

    render() {
        return (
            <div className="loader">
                {
                    (promiseInProgress === true) ? <LinearProgress color="secondary" /> : null
                }
            </div>
        )
    }
}
